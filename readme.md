# Rules

Every turn, a tile can perform one of 3 possible actions:

- `ActionType.stay` Increase it's `stack` by 1. `destination`is ignored.
- `ActionType.move` Move by one space horizontally or vertically. `destination` must be set, must be free, and no other action may have the same destination.
- `ActionType.split` If `stack` is at least 2 create a new tile at `destination` and move half of `stack` to that new tile. `destination` must be set, must be free, and no other action may have the same destination.

If one action in a turn is invalid a bots entire turn will be skipped. No 2 actions may go to the same destination. A tile may perfrom no action, but it is nearly always more beneficial to grow.

If a game is still going after `4 * data.GRID_SIZE * date.GRID_SIZE` turns it will end. This should never happen outside of stalemate.

Some number of obstacles will be placed on the board. They will always be `data.Tile(owner=4, stack=0)` set. Obstacles will be rotationally symetric. They are rendered
black in the preview. The edge of the board is always free of obstacles.

# Programming a bot

Bot colors are relative. Your tiles will always be team 0 and start in the top left corner. The input you get and actions you perform will be rotated to match the actual colors.

You can use `for (x,y) in grid` as a easy shorthand for iterating over every tile in the grid. You can use `grid[x,y]` as a easy shorthand for getting a tile at a specific location.

Care must be taken not to move two seperate tiles to the same location. This will void your turn.

## Example Bot

```py
import data

def expand_bot(grid: data.Grid) -> list[data.Action]:
    """Just splits"""

    def neighbors(t: tuple[int, int]):
        yield (t[0] + 1, t[1])
        yield (t[0], t[1] + 1)
        yield (t[0] - 1, t[1])
        yield (t[0], t[1] - 1)

    actions = []
    reached: set[tuple[int, int]] = set()
    for cell in grid:
        if grid[cell].owner == 0:
            if grid[cell].stack < 2:
                actions.append(
                    data.Action(action_type=data.ActionType.stay, source=cell)
                )
            else:
                try:
                    destination = next(
                        i
                        for i in neighbors(cell)
                        if grid[i].owner is None and i not in reached
                    )
                except StopIteration:
                    actions.append(
                        data.Action(action_type=data.ActionType.stay, source=cell)
                    )
                else:
                    reached.add(destination)
                    actions.append(
                        data.Action(
                            action_type=data.ActionType.split,
                            source=cell,
                            destination=destination,
                        )
                    )
    return actions
```

# Running the controller yourself

This project uses poetry. If you haven't already run:

```bash
pip install poetry
```

Install dependencies:

```bash
poetry install
```

If your IDE or terminal can't find the virutalenvironment itself you can start it manually using:

```bash
poetry shell
```

Then start the renderer like this:

```bash
python renderer.py
```

Hold SPACE to speed up the simulation. Results will be saved in `output.html` and `screenshots/result.json`

# Adding your bot for testing

- Add your bot to the `bots` folder
- Add a line to `competitors.py` like `from bots.my_file import my_bot`
- Add a line like `my_bot,` to the list of participating bots
- Comment out any bots you don't care about to reduce the run time
- run `python renderer.py` to see your bot in action.