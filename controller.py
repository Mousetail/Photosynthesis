import pygame
import data
import itertools
import pygame.gfxdraw
import typing


def are_close(d1: tuple[int, int], d2: tuple[int, int]) -> bool:
    return (
        (d1[0] == d2[0] or d1[1] == d2[1])
        and abs(d1[0] - d2[0]) <= 1
        and abs(d1[1] - d2[1]) <= 1
    )


def validate_actions(grid: data.Grid, actions: list[data.Action], owner: int):
    visited_locations: set[tuple[int, int]] = set()
    sources: set[tuple[int, int]] = set()

    ok = True
    for action in actions:
        if action.action_type == data.ActionType.stay:
            ok = ok and action.destination is None
        elif action.action_type == data.ActionType.move:
            ok = ok and (
                action.destination is not None
                and are_close(action.source, action.destination)
                and action.destination not in visited_locations
                and grid[action.destination].owner is None
            )
            if action.destination is not None:
                visited_locations.add(action.destination)
        elif action.action_type == data.ActionType.split:
            ok = ok and (
                action.destination is not None
                and are_close(action.source, action.destination)
                and action.destination not in visited_locations
                and grid[action.source].stack is not None
                and grid[action.source].stack >= 2
                and grid[action.destination].owner is None
            )
            if action.destination is not None:
                visited_locations.add(action.destination)
        ok = ok and action.source not in sources
        ok = ok and grid[action.source].owner == owner

        if not ok:
            print("Invalid action: ", action)
            return

        sources.add(action.source)
    return ok


def apply_actions(grid: data.Grid, actions: list[data.Action], owner: int):
    destinations = {i.destination: i for i in actions if i.destination is not None}
    sources = {i.source: i for i in actions}
    staying_tiles = {
        i.source: i for i in actions if i.action_type == data.ActionType.stay
    }
    assert len(destinations) + len(staying_tiles) == len(
        actions
    ), f"{len(destinations)=} {len(staying_tiles)=} {len(actions)=}"

    def get_field_value(pos: tuple[int, int]):
        if grid[pos]:
            if pos in sources and sources[pos].action_type == data.ActionType.move:
                return data.Tile()
            elif pos in sources and sources[pos].action_type == data.ActionType.split:
                return data.Tile(owner=owner, stack=grid[pos].stack // 2)
            elif pos in staying_tiles:
                return data.Tile(owner=owner, stack=grid[pos].stack + 1)
            else:
                return grid[pos]
        if pos in destinations:
            if destinations[pos].action_type == data.ActionType.split:
                return data.Tile(
                    owner=owner, stack=(grid[destinations[pos].source].stack + 1) // 2
                )
            elif destinations[pos].action_type == data.ActionType.move:
                return grid[destinations[pos].source]

        return grid[pos]

    return data.Grid(
        tiles=[
            [get_field_value((i, j)) for i in range(data.GRID_SIZE)]
            for j in range(data.GRID_SIZE)
        ],
        turns=grid.turns,
    )
