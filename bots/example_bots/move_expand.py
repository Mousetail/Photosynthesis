import data
import typing


def move_expand_bot(grid) -> list[data.Action]:
    """Moves towards (2,2)"""

    def neighbors(t: tuple[int, int]) -> typing.Generator[tuple[int, int], None, None]:
        yield (t[0] + 1, t[1])
        yield (t[0], t[1] + 1)
        yield (t[0] - 1, t[1])
        yield (t[0], t[1] - 1)

    def distance(t1: tuple[int, int], t2: tuple[int, int]) -> int:
        return abs(t1[0] - t2[0]) + abs(t1[1] - t2[1])

    def has_closer_neighbors(t: tuple[int, int]) -> typing.Optional[tuple[int, int]]:
        for n in neighbors(t):
            if (
                not grid[n]
                and n not in reached
                and distance(n, (2, 2)) < distance(t, (2, 2))
            ):
                return n
        return None

    actions = []
    reached: set[tuple[int, int]] = set()
    for cell in grid:
        if grid[cell].owner == 0:
            if (
                (n := has_closer_neighbors(cell))
                and grid[n].owner is None
                and n not in reached
            ):
                actions.append(
                    data.Action(
                        action_type=data.ActionType.move, source=cell, destination=n
                    )
                )
                reached.add(n)
            elif grid[cell].stack < 2:
                actions.append(
                    data.Action(action_type=data.ActionType.stay, source=cell)
                )
            else:
                try:
                    destination = next(
                        i
                        for i in neighbors(cell)
                        if grid[i].owner is None and i not in reached
                    )
                except StopIteration:
                    actions.append(
                        data.Action(action_type=data.ActionType.stay, source=cell)
                    )
                else:
                    reached.add(destination)
                    actions.append(
                        data.Action(
                            action_type=data.ActionType.split,
                            source=cell,
                            destination=destination,
                        )
                    )
    return actions
