import data
import typing


def go_after_leader_bot(grid) -> list[data.Action]:
    """Moves and splits towards the bot with the highest score"""

    scores = {i: grid.get_score(i) for i in range(4)}
    scores[0] = 0  # Don't go after myself

    def get_score_in_direction(start: tuple[int, int], direction: tuple[int, int]):
        pos = (start[0] + direction[0], start[1] + direction[1])
        d = 0
        while grid[pos].owner is None or grid[pos].owner in {0, 4}:
            pos = (pos[0] + direction[0], pos[1] + direction[1])
            d += 1
        return scores.get(grid[pos].owner, 0)

    def neighbors(t: tuple[int, int]) -> typing.Generator[tuple[int, int], None, None]:
        n = [
            (t[0] + 1, t[1]),
            (t[0], t[1] + 1),
            (t[0] - 1, t[1]),
            (t[0], t[1] - 1),
        ]
        n.sort(
            key=lambda i: get_score_in_direction(t, (i[0] - t[0], i[1] - t[1])),
            reverse=True,
        )
        yield from n

    def distance(t1: tuple[int, int], t2: tuple[int, int]) -> int:
        return abs(t1[0] - t2[0]) + abs(t1[1] - t2[1])

    def has_closer_neighbors(t: tuple[int, int]) -> typing.Optional[tuple[int, int]]:
        for n in neighbors(t):
            if not grid[n] and n not in reached:  # and n not in blacklisted:
                return n
        return None

    actions = []
    reached: set[tuple[int, int]] = set()
    blacklisted = {(0, 0)}
    for cell in grid:
        if grid[cell].owner == 0:
            if (
                grid[0, 0].owner == 0
                and (n := has_closer_neighbors(cell))
                and grid[n].owner is None
                and n not in reached
            ):
                actions.append(
                    data.Action(
                        action_type=data.ActionType.move, source=cell, destination=n
                    )
                )
                reached.add(n)
            elif grid[cell].stack < 2:
                actions.append(
                    data.Action(action_type=data.ActionType.stay, source=cell)
                )
            else:
                try:
                    destination = next(
                        i
                        for i in neighbors(cell)
                        if grid[i].owner is None and i not in reached
                    )
                except StopIteration:
                    actions.append(
                        data.Action(action_type=data.ActionType.stay, source=cell)
                    )
                else:
                    reached.add(destination)
                    actions.append(
                        data.Action(
                            action_type=data.ActionType.split,
                            source=cell,
                            destination=destination,
                        )
                    )
    return actions
