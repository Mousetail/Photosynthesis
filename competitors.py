from bots.example_bots.expand import expand_bot
from bots.example_bots.free_space import free_space_bot
from bots.example_bots.occupied_space import occupied_space_bot
from bots.example_bots.move_expand import move_expand_bot
from bots.example_bots.edge_move import edge_move_bot
from bots.example_bots.double_edge_move import double_edge_move_bot
from bots.example_bots.move_expand_corner import move_expand_corner_bot
from bots.example_bots.move_expand_corner_limitless import (
    move_expand_corner_limitless_bot,
)
from bots.example_bots.centered_expand import centered_expand_bot
from bots.example_bots.edge_expand import edge_expand_bot
from bots.example_bots.go_after_leader import go_after_leader_bot
from bots.example_bots.go_after_loser import go_after_loser_bot
from bots.example_bots.smart_outline import outline_bot

import data
import typing

competitors: list[typing.Callable[[data.Grid], list[data.Action]]] = [
    move_expand_corner_bot,
    edge_move_bot,
    go_after_leader_bot,
    go_after_loser_bot,
    free_space_bot,
    move_expand_corner_limitless_bot,
    edge_expand_bot,
    expand_bot,
    double_edge_move_bot,
    outline_bot,
    centered_expand_bot,
    move_expand_bot,
    occupied_space_bot,
]
